export class Employee{
    public constructor(
    public Id?: number,
    public Name?: string,
    public Phone?: string,
    public City?: string,
    public Address1?: string,
    public Address2?: string,
    public PostalCode?: number){
    }
}